package foodonate.ayushi.com.foodonate;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

/**
 * Created by Khandelwal on 08-12-2016.
 */

public class Food_Add_Donate extends AppCompatActivity {

    private EditText f_name, f_price, f_no_people, f_description, c_name,c_address, c_phone;
    private TextInputLayout l_f_name, l_f_price, l_f_no_people, l_c_name, l_c_address, l_c_phone;
    private Button done;
    double lat=0;
    double lng = 0;
    String address = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_add_donate);

        Firebase.setAndroidContext(this);

        l_f_name = (TextInputLayout) findViewById(R.id.til_fa_name);
        l_f_price = (TextInputLayout) findViewById(R.id.til_fa_price);
        l_f_no_people = (TextInputLayout) findViewById(R.id.til_fa_no_people);
        l_c_name = (TextInputLayout) findViewById(R.id.til_fac_name);
        l_c_address = (TextInputLayout) findViewById(R.id.til_fac_address);
        l_c_phone = (TextInputLayout) findViewById(R.id.til_fac_phone);

        f_name = (EditText) findViewById(R.id.et_fa_name);
        f_price = (EditText) findViewById(R.id.et_fa_price);
        f_no_people = (EditText) findViewById(R.id.et_fa_no_people);
        f_description = (EditText) findViewById(R.id.et_fa_description);
        c_name = (EditText) findViewById(R.id.et_fac_name);
        c_address = (EditText) findViewById(R.id.et_fac_address);
        c_phone = (EditText) findViewById(R.id.et_fac_phone);

        f_name.addTextChangedListener(new MyTextWatcher(f_name));
        c_name.addTextChangedListener(new MyTextWatcher(c_name));
        c_phone.addTextChangedListener(new MyTextWatcher(c_phone));


        done = (Button) findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();

                Firebase ref = new Firebase(Config.FIREBASE_URL);
                Food food = new Food();
                food.setName(f_name.getText().toString());
                food.setDescription(f_description.getText().toString());
                food.setCan_feed(f_no_people.getText().toString());
                food.setPrice(f_price.getText().toString());
                food.setUser_name(c_name.getText().toString());
                food.setUser_address(lat+"_"+lng);
                food.setUser_phone(c_phone.getText().toString());


                //Storing Values to Firebase
                ref.child("Food").push().setValue(food);
                finish();

                //Value event listener for realtime data update
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

//                        for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
//                        {
//                            Food food1 = postSnapshot.getValue(Food.class);
//
//                            String string = "Food : " + food1.getName() + "\n Desc " + food1.getDescription();

                            Log.i("FOOD VALUE : ", dataSnapshot.toString());
//                            System.out.println("The FooDonate " + string);

//                        }
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {
                        System.out.println("The FooDonate failed: " + firebaseError.getMessage());

                    }
                });
            }
        });



        try{

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {


                    GPSService mGPSService = new GPSService(Food_Add_Donate.this);
                    mGPSService.getLocation();

                    if (mGPSService.isLocationAvailable == false) {

                        // Here you can ask the user to try again, using return; for that
//                        Toast.makeText(this, "Your location is not available, please try again.", Toast.LENGTH_SHORT).show();
//                        return;

                        // Or you can continue without getting the location, remove the return; above and uncomment the line given below
                         address = "Location not available";
                    } else {

                        // Getting location co-ordinates
                        double latitude = mGPSService.getLatitude();
                        double longitude = mGPSService.getLongitude();
//                Toast.makeText(this, "Latitude:" + latitude + " | Longitude: " + longitude, Toast.LENGTH_LONG).show();

                        address = mGPSService.getLocationAddress();

//                tvLocation.setText("Latitude: " + latitude + " \nLongitude: " + longitude);
                        lat = latitude;
                        lng = longitude;

                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            c_address.setText(address);
                        }
                    });

//                    Toast.makeText(this, "Your address is: " + address, Toast.LENGTH_SHORT).show();

                    // make sure you close the gps after using it. Save user's battery power
                    mGPSService.closeGPS();

                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void submitForm() {
        if (!validateFoodName()) {
            return;
        }

        if (!validateContactName()) {
            return;
        }

        if (!validateContactPhone()) {
            return;
        }

        Toast.makeText(getApplicationContext(), "Thank You!", Toast.LENGTH_SHORT).show();
    }

    private boolean validateFoodName() {
        if (f_name.getText().toString().trim().isEmpty()) {
            l_f_name.setError(getString(R.string.err_food_name));
            requestFocus(f_name);
            return false;
        } else {
            l_f_name.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateContactName() {
        if (c_name.getText().toString().trim().isEmpty()) {
            l_c_name.setError(getString(R.string.err_food_contact_name));
            requestFocus(c_name);
            return false;
        } else {
            l_c_name.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateContactPhone() {
        if (c_phone.getText().toString().trim().isEmpty()) {
            l_c_phone.setError(getString(R.string.err_food_contact_phone));
            requestFocus(c_phone);
            return false;
        } else {
            l_c_phone.setErrorEnabled(false);
        }

        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.et_fa_name:
                    validateFoodName();
                    break;
                case R.id.et_fac_name:
                    validateContactName();
                    break;
                case R.id.et_fac_phone:
                    validateContactPhone();
                    break;
            }
        }
    }

    }
