package foodonate.ayushi.com.foodonate;

import static android.R.attr.name;

/**
 * Created by Ayushi on 19-12-2016.
 */

public class Food {

    public Food()
    {

    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCan_feed() {
        return can_feed;
    }

    public void setCan_feed(String can_feed) {
        this.can_feed = can_feed;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_address() {
        return user_address;
    }

    public void setUser_address(String user_address) {
        this.user_address = user_address;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    private String description;
    private String price;
    private String can_feed;
    private String user_name;
    private String user_address;
    private String user_phone;

}
